import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  RefreshControl
} from 'react-native';
import StatusBar from './StatusBar'
import * as WeatherService from './WeatherService'

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      refreshing: false,
      currentPosition: {
        longitude: 0,
        latitude: 0
      },
      forecast: {}
    }
  }

  componentDidMount() {
    this.updateWeather();
  }

  setBussy(isRefreshing) {
    this.setState((prevState, props) => {
      return {
        refreshing: isRefreshing,
      }
    });
  }

  async updateWeather() {
    this.setBussy(true);

    navigator.geolocation.getCurrentPosition(async (position) => {
      this.setState((prevState, props) => {
        return {
          currentPosition: {
            longitude: position.coords.longitude,
            latitude: position.coords.latitude
          }
        };
      });

      let forecast = await WeatherService.GetWeather(this.state.currentPosition.longitude, this.state.currentPosition.latitude);

      this.setState((prevState, props) => {
        return {
          forecast: forecast
        }
      });

      this.setBussy(false);
    }, error => {
      this.setState((prevState, props) => {
        return { error: JSON.stringify(error) };
      });
      this.setBussy(false);
    }, {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      });
  }

  render() {
    return (
      <ScrollView style={styles.container} refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={() => this.updateWeather()}
        />
      }>
        <StatusBar barStyle="dark-content"/>

        {this.state.forecast && !this.state.forecast.error && <View>
        <Text style={styles.big}>Aktualnie:</Text>
        <Text style={styles.superBig}>{this.state.forecast && this.state.forecast.currently && this.state.forecast.currently.temperature}℃</Text>

        <Text style={styles.big}>Dzisiaj:</Text>
        <Text style={styles.summaryText}>{this.state.forecast && this.state.forecast.hourly && this.state.forecast.hourly.summary}</Text>

        <Text style={styles.big}>Następne 48 godzin:</Text>

        {
          this.state.forecast && this.state.forecast.hourly && this.state.forecast.hourly.data &&
          this.state.forecast.hourly.data.map((item, index) => <View key={index}
            style={{
              flexDirection: 'row',
              padding: 10,
              borderWidth: 1,
              borderRadius: 2,
              borderColor: '#ddd',
              borderBottomWidth: 0,
              marginLeft: 5,
              marginRight: 5,
              marginTop: 10
            }}>
            <View style={{ flex: 0.2 }}><Text style={styles.big}>{new Date((item.time * 1000 + 1000 * 60 * 60 * this.state.forecast.offset)).getUTCHours()}:00</Text></View>
            <View style={{ flex: 0.8 }} >
              <Text>Stan pogody: {item.summary}</Text>
              <Text>Temperatura: {item.temperature} </Text>
            </View>
          </View>)
        }
        </View>}

        {this.state.forecast && this.state.forecast.error && <View>
          <Text>Error:</Text>
          <Text>{this.state.forecast.error}</Text>}
        </View>}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  elementShadow: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  },
  superBig: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 36,
    color: '#333333',
  },
  big: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    color: '#333333',
  },
  medium: {
    fontSize: 17
  },
  container: {
    backgroundColor: '#F5FCFF',
  },
  summaryText: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
