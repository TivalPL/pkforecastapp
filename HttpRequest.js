const HTTP_METHODS = {
    GET: "GET",
    POST: "POST",
    DELETE: "DELETE",
    PUT: "PUT"
}

export async function Get(url, headers) {
    try {
        let response = await fetch(url, {
            headers: headers,
            method: HTTP_METHODS.GET
        });
        let responseJson = response.json();
        return responseJson;
    } catch (error) {
        console.error(error);
    }
}

export async function Post(url, headers, body) {
    try {
        let response = await fetch(url, {
            headers: headers,
            body: body,
            method: HTTP_METHODS.POST
        });
        let responseJson = response.json();
        return responseJson;
    } catch (error) {
        console.error(error);
    }
}