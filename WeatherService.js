import config from './config';
import * as request from './HttpRequest';
import EncodeQueryParameters from './QueryStringBuilder';

const BASE_URL = "https://api.darksky.net"

export async function GetWeather(latitude, longitude) {
    let parameters = EncodeQueryParameters({
        "units": "auto",
        "lang": "pl"
    });

    var requestUrl = `${BASE_URL}/forecast/${config.DarkSkyApiKey}/${latitude},${longitude}${parameters}`;
    let response = request.Get(requestUrl);

    return response;
}